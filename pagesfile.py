FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>URL: </label>
        <input type="text" name="url" required>
      </div>
      <div>
        <input type="submit" value="Shorten URL">
      </div>
    </form>
"""

STANDARD_PAGE = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <div>
            {form}
        </div>
        <div>
            <p>Shortened URL dictionary:</p> {url_list}
        </div>
      </body>
    </html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>PAOE NOT FOUND.</p>
    </div>
  </body>
</html>
"""

# PAGE_NOT_ALLOWED = """
# <!DOCTYPE html>
# <html lang="en">
#   <body>
#     <p>Method not allowed: {method}.</p>
#   </body>
# </html>
# """
#
# PAGE_UNPROCESABLE = """
# <!DOCTYPE html>
# <html lang="en">
#   <body>
#     <p>Unprocesable POST: {body}.</p>
#   </body>
# </html>
# """